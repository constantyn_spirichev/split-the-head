

var fs = require('fs');
var jsdom = require("jsdom");
//var jquery = fs.readFileSync("./node_modules/jquery/dist/jquery.min.js", "utf-8");

var config = {};

config.rootNode = "teil";
config.childeNode = "h-kap";
config.wrapperNode = "k3";
config.srcFolder = "src";
config.targetFolder = "target";

//var data = fs.readFileSync('src/Alhambra sem45.15_ES.xml').toString();

  function readFile(file, index){
    var data = fs.readFileSync(config.srcFolder+"/"+file).toString();
        //createFolder(config.targetFolder+"/"+file);
        jsdom.env(data, ["http://code.jquery.com/jquery.js"], processDocument(file));
        console.log("|");

  }

  function createFolder(folder){
      fs.stat(folder, function(err, stats){
         if(err){
           fs.mkdir(folder, (err2) => {
                if(err2)
                throw new Error("Folder not created: ", err2);
                console.log("'%s' Folder created", folder);
              })
         }
      });
  }

  function processDocument(documentName){
    
    var path = config.targetFolder+"/"+ documentName.replace(/\.xml$/, "")+"/";
        createFolder(path);

    return function(err, window){
            if(err){
              throw new Error("Document parser error: ", err);
            }

            var rootElements = window.$(config.rootNode);

                rootElements.each(function(index, item){

                  var childeNodes = window.$(item).find(config.childeNode);

                      childeNodes.each(function(indx, itm){
                        var fileName = path+config.rootNode+"-"+index+"-"+config.childeNode+"-"+indx+".xml";
                        var data = '<?xml version="1.0" encoding="UTF-8"?>\r\n';

                            data += '<!DOCTYPE k3 PUBLIC "-//DOSCO VW//DTD K3 R5//DE" "k3.dtd">\r\n';
                            data += "<k3>\r\n" + "<dokument>\r\n";
                            data += itm.innerHTML;
                            data += "</k3>\r\n" + "</dokument>";

                          fs.writeFileSync(fileName, data);
                          console.log("%s saved", fileName);

                      }).remove();

                });

                //window.rootElements.find();
                var mainText = '<?xml version="1.0" encoding="UTF-8"?>\r\n';
                    mainText += '<!DOCTYPE k3 PUBLIC "-//DOSCO VW//DTD K3 R5//DE" "k3.dtd">\r\n';
                    mainText += "<dokument>\r\n";
                    mainText += window.$(config.wrapperNode).get(0).outerHTML;
                    mainText += "\r\n</dokument>";


                fs.writeFileSync(path+documentName, mainText);
                console.log("%s was saved", documentName);
                console.log("==================================\r\n");
          }
  }

  createFolder(config.targetFolder);

  fs.readdir(config.srcFolder, function readdir(err, item){
      if(err){
        throw new Error("Source folder not exists")
      }
      item.map(readFile);
  });
